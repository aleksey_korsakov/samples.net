﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Enums
{
    [Flags]
    public enum HumanContainer
    {
        LeftArm = 1,
        RightArm = 2,
        LeftLeg = 4,
        RightLeg = 8,
        Head = 16,
        Torso = 32,
    }

    public class Human
    {
        HumanContainer parts;
        public bool IsHumanHasHead { get { return parts.HasFlag(HumanContainer.Head); } }

        public Human()
        {
            parts = HumanContainer.Head
                    | HumanContainer.LeftArm
                    | HumanContainer.LeftLeg
                    | HumanContainer.RightArm
                    | HumanContainer.RightLeg
                    | HumanContainer.Torso;
        }
    }
}
