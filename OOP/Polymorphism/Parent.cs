﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Inheritance
{
    public class Parent
    {
        public virtual string GetName { get { return "Parent"; } }
    }
}
