﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Inheritance
{
    public class Child2 : Parent
    {
        public override string GetName { get { return "Child2"; } }
    }
}
