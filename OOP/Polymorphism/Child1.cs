﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP.Inheritance
{
    public class Child1 : Parent
    {
        public new string GetName { get { return "Child1"; } }
    }
}
