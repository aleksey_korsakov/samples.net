﻿using OOP.Enums;
using OOP.Inheritance;
using System;

namespace OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            Human vasya = new Human();
            Console.WriteLine("Enums: Human.IsHumanHasHead - " + vasya.IsHumanHasHead);
            Console.WriteLine("------------------------------------");

            Parent child = new Child1();
            Console.WriteLine(((Child1)child).GetName);
            Console.WriteLine(child.GetName);
            Console.WriteLine(child.GetName);

            Parent child2 = new Child2();
            Console.WriteLine(((Child2)child2).GetName);
            Console.WriteLine(child2.GetName);

        }
    }
}
